import requests
import random # only used to generate example data

# credentials for belfast.pvos.org (for this particular sensor feed)
public_key = "[YOUR PUBLIC KEY]"
private_key = "[YOUR PRIVATE KEY]"

# these will stay fixed:
base_url = "http://belfast.pvos.org/data/"
full_url = base_url+public_key

# example data:
distance = random.randint(10,20)

# the JSON object we'll be POST-ing to 'full_url' ...
# NOTE: we must include the private_key as one of the parameters;
# and 'distance_meters' is one of several possible parameters in the postgres database.
myobj = {"private_key":private_key, "distance_meters":distance}

x = requests.post(full_url, data = myobj)
print (distance)
print(x.text)
